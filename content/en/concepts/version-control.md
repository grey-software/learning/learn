---
title: Version Control Systems
description: "Learn how software developers around the world collaborate using version control systems"
position: 3
category: Essential Concepts
---

<author name="Arsala" desc="President at Grey Software" linkedin-url="https://linkedin.com/in/ArsalaBangash" twitter="arsalagrey" avatar-url="https://gitlab.com/uploads/-/system/user/avatar/2274539/avatar.png" gitlab-url="https://gitlab.com/ArsalaBangash" github-url="https://github.com/ArsalaBangash" ></author>

### What is a Version Control System (VCS)?

A VCS tracks the history of changes as people and teams collaborate on projects together.

As the project evolves, teams can run tests, fix bugs, and contribute new code with the confidence that they can recover any version.

Developers can review the project's history to find out:

- What changes were made
- Who made the changes
- When were the changes made
- Why were changes needed

### Collaboration

A VCS allows multiple people to work on the same set of files in structured harmony.

Messaging your teammates about which file you're changing and telling them to keep their fingers off is not the optimal workflow. 

Neither is storing .zip snapshots of your code on a shared online drive.

With a VCS, team members can work on any project file on their local version and merge their changes into a shared version. 

The latest version of a file or the whole project is always in a shared location managed by the VCS.

### Storing Versions

Saving a version of your project after making changes is an essential habit. 

But without a VCS, this becomes tedious and confusing very quickly. 

How much do we save? 
Only the changed files or the complete project?
How do we name these versions? 

If you're a very organized person, you might be able to stick to a coherent naming system.

How can we know exactly what files changed in these versions? It is problematic to expect humans to carefully document each change consistently.

A version control system acknowledges that there is only one project. 

Therefore, there's only one version on your disk that you're currently working on. 

All the past versions and variants of your project are available through the VCS.

### Restoring Previous Versions

Being able to restore older versions of a file or the whole project means you can't mess up! 

If the changes you make prove to be garbage, you can undo them with a few commands. Knowing this should make you more relaxed when working on a project.

### Understanding What Happened

Every time you save a new version of your project, your VCS requires you to briefly describe what was changed to help you understand how your project evolved between versions.

You can see also see what file contents were changed.

### Backup

A distributed VCS like Git can also act as a backup. 

Every team member has a repository of the project on their disk with its entire version history. 

All you need to recover your entire project is one copy of the git repository.

## What is Distributed Version control System

A distributed version control system (DVCS) is a type of version control where the complete codebase — including its full version history — is mirrored on every developer's computer.

## Which VCS?

Which VCS do we use in practice? At Grey Software, we use Git, like the overwhelmingly vast majority of software development companies.

[Read more about Git!](/git)
