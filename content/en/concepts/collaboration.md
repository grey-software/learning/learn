---
title: Collaboration
description: "Learn how software developers around the world collaborate"
position: 2
category: Essential Concepts
---

<author name="Arsala" desc="President at Grey Software" linkedin-url="https://linkedin.com/in/ArsalaBangash" twitter="arsalagrey" avatar-url="https://gitlab.com/uploads/-/system/user/avatar/2274539/avatar.png" gitlab-url="https://gitlab.com/ArsalaBangash" github-url="https://github.com/ArsalaBangash" ></author>

## How do software developers collaborate?

When you're working with software developers from around the world on a project, you need to know:

- What changes were made since your last collaboration
- Who made those changes
- When were those changes made
- Why were those changes needed

Messaging your teammates about which file you're changing and telling them to keep their fingers off is not the optimal workflow. Neither is storing .zip snapshots of your code on a shared online drive.

To get around this issue, we use a version control system (or a VCS).

A VCS allows multiple people to work on the same set of files in structured harmony.

Team members can work on any project file on their local version and merge their changes into a shared version.

The latest version of a file or the whole project is always in a shared location managed by the VCS.

[Read more about version control systems.](/version-control)
