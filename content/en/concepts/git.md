---
title: Git
description: "Learn about how Git enables software developers around the world to collaborate."
position: 4
category: Essential Concepts
---

Git is a free and open-source distributed version control system used by the vast majority of software development companies.

Git is commonly used for both open source and commercial software development, with significant benefits for individuals, teams, and businesses:

- Git lets developers see the entire timeline of their changes, decisions, and progression of any project in one place. 

- Developers can work concurrently in any time zone while maintaining source code integrity.

- Teams using a Git platform such as Github or Gitlab can break down communication barriers between teams and keep them focused on doing their best work.

<alert type="info">
Why do we use both Github and Gitlab? We primarily use Gitlab for development and collaboration because we found it easier to remotely collaborate on multiple projects across an organization compared to Github. We still use GitHub because of its vast community, classroom platform, and sponsorships program!
</alert>
