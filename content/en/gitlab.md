---
title: Overview
description: Learn about how we manage projects at Grey Software with GitLab.
category: Gitlab
position: 8
---

GitLab is a complete DevOps platform that brings development, operations, and security teams into a single application.

It helps teams accelerate software delivery from weeks to minutes while reducing development costs and security risks.

## Gitlab at Grey Software

At Grey Software, we use Gitlab to

- Collaborate on ideas, solve problems, and plan work via [Issues](/gitlab/issues)
- Organize our work into categories such as complexity, status, and priority via [Labels](/gitlab/labels)
- Track the progress of our projects across a specific time period via [Milestones](/gitlab/milestones)
- Outline our higher level objectives via [Epics](/gitlab/epics)
- Visualize our objectives in a timeline via [Roadmaps](/gitlab/roadmaps)
- Communicate and collaborate in Markdown by using [Comments & Threads](/gitlab/comments-threads)

> Official Resources:
>
> - [Agile planning with GitLab](https://about.gitlab.com/solutions/agile-delivery/)
> - [How to use Gitlab](https://docs.gitlab.com/ee/topics/use_gitlab.html)
> - [How to use GitLab for Agile software development](https://about.gitlab.com/blog/2018/03/05/gitlab-for-agile-software-development/)
