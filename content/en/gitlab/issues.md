---
title: Issues
description: Learn about how we manage issues with GitLab.
category: Gitlab
position: 9
---

Issues are an essential building block of agile software development!

While they can be customized to the needs of a project, they are primarily used to:

- Plan, track, and communicate work across a project or organization.
- Discuss ideas for enhancements or features.
- Report bugs and handle support requests.

## Issues at Grey Software

![](https://i.imgur.com/qkM0sKM.png)

[This issue](https://https://gitlab.com/grey-software/resources/-/issues/14)  was created to brainstorm the Gitlab management document! 

It eventually gave birth to this Gitlab guide you're currently reading. 

### 🔍 Let's Break An Issue Down

1. **Title** : Indicates what the issue is about via a one-sentence overview.
2. **Body** : Explains the issue in detail, including it's background, notes, and tasks.
3. **Assignees** : Indicates to whom this issue is assigned. An issue can be assigned to more than one person.
4. [Epic](/gitlab/epics) : We assign Epics, to better manage groups of related issues across an organization.
5. [Milestone](/gitlab/milestones) : We assign milestones to better track an issue as part of a sprint or objective.
6. **Iteration** : Iterations are a way to track issues over a shorter period of time as compared to milestones.
7. **Due date** : Tracks the issue's deadlines to make sure it's addressed on time.
8. [Labels](/gitlab/labels) : Indicates the stage, status, level and type of an issue.
9. **Weights** : Indicates how much time, value, or complexity a given issue has/costs.
10. [Comments](/gitlab/comments-threads) : Team members can collaboratively address the issue by posting comments in its thread.

**Another Example**

This issue was created to track our new landing website

![](https://i.imgur.com/OU0xcTn.png)

We invite you to check our active issues to see what's happening at Grey Software!

<cta-button text="Our Issues" link="https://gitlab.com/groups/grey-software/-/issues"> </cta-button>


## How we process issues at Grey Software

<flow-chart name="issue-lifecycle"></flow-chart>

At Grey Software we have developed a lifecycle for issues with the help of labels.

Each stage of the lifecycle has a specific label associated with it, which allows you to easily trace the issue's status.

[Learn More about Gitlab Labels](/gitlab/labels)

## How to Create Issues at Grey Software

![](https://i.imgur.com/GJ0p7Er.png)

![](https://i.imgur.com/snIJydV.png)

### Title

The title of the issue should be precise and unambiguous to clearly show the issue's objective.

### Description

Different issues require different kinds of descriptions.

![](https://i.imgur.com/hmzUcDx.png)

![](https://i.imgur.com/hgAVRxa.png)

![](https://i.imgur.com/7M9tUvd.png)

![](https://i.imgur.com/42zltE0.png)

![](https://i.imgur.com/73oDWna.png)

We use any of the following headings to explain the issue.

- **Background** : Background information that is necessary to explain the issue.
- **Motivation** : What motivation has led you to create this issue.
- **Overview** : What is the purpose of this issue.
- **Notes** : Additional links and supplementary information, any linked issues.
- **Tasks** : Issues are often made up of sub-tasks. Writing them out can help you better understand how to tackle this issue and determine its weight.


```
- [ ] **8 points** Implement functionality to block ads on Google

```

The description fully supports GitLab Flavored Markdown, allowing formatting options like:

Creating subtasks that Gitlab can track via checkboxes
Mentioning other issues via their id (e.g. #46).

### Issue Weights

> - [Issue Weights in GitLab](https://docs.gitlab.com/ee/user/project/issues/issue_weight.html/)

The particular way we use weights at Grey software is by breaking apart an issue into tasks, writing out those tasks as markdown checklist items, and then once we have the final weight, we assign it to an issue.

Weights are assigned on the basis of a task's time, value, or complexity.

In the example given below;
The two tasks are allocated points with respect to their time, value, and complexity.
The total sum of the two task points is 13, which is the issue's weight.

As an added bonus, you can see the total sum of all issues on the milestone page.

![](https://i.imgur.com/6JD4QQd.png)

**Weights are not set in stone.**

You can change the weight of an issue as you discover more about the solution, and it is also OK to assign weights retrospectively.

### Assignees

If you know you will be working on the issue then add yourself as assignee, otherwise you could leave it blank and wait for one of the maintainers to assign it to somebody after inspecting and planning the issue.

### Epic

Select an [epic](/gitlab/epics) for the issue.
This is usually done by the maintainers so its okay if you dont know what epic to select.

### Milestone

Select a [milestone](/gitlab/milestones) for the issue.
This is usually done by the maintainers so its okay if you dont know what milestone to select.

### Due Date

You can leave this blank because we manage issues' deadlines using milestones and iterations.

## Issues List

The issues list is a UI where we can run complex search queries to filter out issues and edit multiple issues at once.

![](https://i.imgur.com/E9JaMjO.png)

## Issue Boards

Issue boards allow us to plan, organize, and visualize a workflow for a feature or product release.

They help to visualize how issues are being processed, and are inspired by Kanban boards.

The following example issue boards allow us to visualize all of Grey Software's issues bucketed by their priority or position in the Issues Lifecycle.

### Lifecycle Board

![](https://i.imgur.com/q1zBirZ.png)

### Priority Board

![](https://i.imgur.com/tDgmTjC.png)
