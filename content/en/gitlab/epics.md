---
title: Epics
description: Learn about how we use Gitlab epics to collaborate.
category: Gitlab
position: 11
---

## Overview

Epics allow you to manage and organize high-level goals and objectives.

If you're a project manager, Epics allow you to organize [issues](/gitlab/issues) and sub-epics that share a common goal.

## How we use Epics at Grey Software

### Create Epic for a project

When we want to develop a project, we create an epic for it.

> [Epic for our Focused Browsing project](https://gitlab.com/groups/grey-software/-/epics/22)

![](https://i.imgur.com/aFRMexx.png)

### Time Frame for the Epic

Every epic has a time frame that anticipates some deliverables at the end .

The time frame of an epic can be fixed or inherited from the [milestones](/gitlab/milestones) assigned to issues in this epic.

If you select **Inherited**:

- **For the start date**: GitLab scans all child epics and issues assigned to the epic, and sets the start date to match the earliest start date found in the child epics or the milestone assigned to the issues.
- **For the due date**: GitLab scans all child epics and issues assigned to the epic, and sets the due date to match the latest due date found in the child epics or the milestone assigned to the issues.

If you select **Fixed**:

- It is then your decision to set the start and due date using your best project management skills.

### Create Issues

We create issues related to the project.

> [Issues related to our Focused Browsing project](https://gitlab.com/grey-software/focused-browsing/-/issues)

<alert type="info"> When all issues in an epic are closed then it has completed successfully .</alert>

## Details in an Epic

![](https://i.imgur.com/pfqaOHS.png)

1. Epic's title.
2. Child epics or issues: two of the child issues are associated with this epic.
3. Start date of the epic.
4. Due date of the epic.
5. With attached labels, we can know the epic's status,stage and priority level.
6. Parent epics: the above epic has two parent epics.

Its Hierarchy is shown below.

<flow-chart name="epic"></flow-chart>

## Relationships Between Epics and Issues are:

- An epic can be the parent of one or more issues.
- An epic can be the parent of one or more child epics

![](https://i.imgur.com/cGXWCiy.png)

## Relationship Between Epic and Milestone

Epic is a larger project or deliverable that you want to get done and [milestone](/gitlab/milestones) is a chunk of time

## Epics at Grey Software

**🎯 [Maintain open source software that people can trust, love, and learn from](https://gitlab.com/groups/grey-software/-/epics/18)**

**🎯 [Be a global OSS Education thought leader](https://gitlab.com/groups/grey-software/-/epics/2)**

**🎯 [Generate sustainable revenue](https://gitlab.com/groups/grey-software/-/epics/16)**

> [We invite you to explore more about our Epics](https://gitlab.com/groups/grey-software/-/epics)

## Epic Board

Epic boards track the existing epics through labels. They appear as cards in vertical lists, organized by their assigned labels.

![](https://i.imgur.com/VHRtteR.png)

> [We invite you to explore more about our Epics' board](https://gitlab.com/groups/grey-software/-/epic_boards)
