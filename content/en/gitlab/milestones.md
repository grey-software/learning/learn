---
title: Milestones
description: Learn about how we use Gitlab milestones to collaborate.
category: Gitlab
position: 12
---

Milestones are a great way to track the progress of issues across a specific time period. 

At Grey Software, we use milestones to track agile sprints or objectives that are greater than an [issue](/gitlab/issues) and smaller than an [epic](/gitlab/epics). 

With milestones, you can see:

- Issues grouped by labels or status.
- To whom issues are assigned to?
- How many issues are completed.

## Milestones at Grey Software

On a high level, a milestone exhibits three major details as follows.
-  The number of issues *i.e,* '6'.
-  The number of merge requests *i.e,* '5'.
-  The status of a milestone *i.e,* '16% complete'.
![](https://i.imgur.com/hPEoLSs.png)

On exploring further, you can see all the essential information  needed to track a milestone. ![](https://i.imgur.com/bWuEtLh.png)

1. Milestone Completion status.
2. Due Date : when a milestone is due.
3. Issue Details : number of open and closed issues.
4. Merge Request Details : number of open and closed merge requests.
5. List of on-going issues and merge requests.
6. List of completed issues and merge requests.

> [ We invite you to explore our active milestones.](https://gitlab.com/groups/grey-software/-/milestones)

## Focused Browsing Milestones

![](https://i.imgur.com/33VA9Z9.png)

> [ We invite you to explore Focused Browsing Milestones to help you get a deeper understanding of how milestones work in a project.](https://gitlab.com/grey-software/focused-browsing/-/milestones?sort=due_date_desc&state=closed)
